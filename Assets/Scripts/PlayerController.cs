﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float speedBall = 0.0f;
    public Text countText;
    public Text winText;

    private Rigidbody rigidbody;
    private int count;

    void Start() {
        rigidbody = GetComponent<Rigidbody>();
        count = 0;
        setCountText();
        winText.text = "";
    }

    void FixedUpdate() {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rigidbody.AddForce(movement * speedBall);
    }

    void OnTriggerEnter(Collider other) {
        if(other.gameObject.CompareTag("Pick Up")) {
            count++;
            other.gameObject.SetActive(false);
            setCountText();
        }
    }

    private void setCountText() {
        countText.text = "Count " + count.ToString();
        if(count >= 13) {
            winText.text = "You win!";
        }
    }
}
